def calc_bit(modus,space):
    if modus==1:
        return space
    elif modus>=2:
        tempspace=int(space)
        for i in range(1,int(modus)-1):
            tempspace=tempspace*1024
        tempspace=tempspace*8
        return tempspace

def calc_x(modus,space,x):
    if modus==x:
        return space
    elif modus==1:
        tempspace = int(space)/8
        for i in range(1,abs(modus-x)):
            tempspace=tempspace/1024
        return tempspace
    elif modus>x:
        tempspace=int(space)
        for i in range(1,abs(modus-x)+1):
            tempspace=tempspace*1024
        return tempspace
    else:
        tempspace=int(space)
        for i in range(1,abs(modus-x)+1):
            tempspace=tempspace/1024
        return tempspace

def print_menu():
    print("1 - Eingabe in Bit")
    print("2 - Eingabe in Byte")
    print("3 - Eingabe in Kibibyte")
    print("4 - Eingabe in Mebibyte")
    print("5 - Eingabe in Gibibyte")
    print("6 - Eingabe in Tebibyte")
    print("0 - Beenden")
    selection=int(input("Modus: "))
    
    return selection

def input_value(a):
    if a==1:
        b="Bit"
    elif a==2:
        b="Byte"
    elif a==3:
        b="Kibibyte"
    elif a==4:
        b="Mebibyte"
    elif a==5:
        b="Gibibyte"
    elif a==6:
        b="Tebibyte"
    elif a==0:
        b="Exit"

    c=int(input("Wert in "+ b +": "))
    return c

def print_result(a,b): 
    print("")
    print("     Bits:     ",calc_bit(a,b))
    print("     Bytes:    ",calc_x(a,b,2))
    print("     Kibis:    ",calc_x(a,b,3))
    print("     Mebis:    ",calc_x(a,b,4))
    print("     Gibis:    ",calc_x(a,b,5))
    print("     Tebis:    ",calc_x(a,b,6))
    print("")

while True:
    try:
        selection=print_menu()
        if selection==0: break
        inputspace=input_value(selection)
        print_result(selection,inputspace)
    except:
        print("ERROR")