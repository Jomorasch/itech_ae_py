def get_bits():
    while True:
        try:
            a = int( input( " Bits? : " ) )
            if a<=0:
                raise Exception
            break
        except:
            print("Only positive, non zero integers are supported!")
    return a

def calc_fullbytes(a):
    return a//8

def calc_restbits(a):
    return a%8

bit = get_bits()
byte = calc_fullbytes( bit )
rest = calc_restbits( bit )

if rest == 0 :
    if byte == 1:
        print( " There is",byte,"Byte in",bit,"bits. " )
    else:
        print( " There are",byte,"Bytes in",bit,"bits. " )
else:
    if byte == 1:
        if rest == 1:
            print( " There are",byte,"Byte and",rest,"bit in",bit,"bits. " )
        else:
            print( " There are",byte,"Byte and",rest,"bits in",bit,"bits. " )
    else:
        if rest == 1:
            print( " There are",byte,"Bytes and",rest,"bit in",bit,"bits. " )
        else:
            print( " There are",byte,"Bytes and",rest,"bits in",bit,"bits. " )
    
