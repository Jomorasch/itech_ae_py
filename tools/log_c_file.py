import shutil,datetime,socket,string
from pathlib import Path
from ctypes import windll

now=datetime.datetime.now()
isodatetime=now.strftime("%Y-%m-%d %H:%M")
hostName=socket.gethostname()

def get_drives():
    drives = []
    bitmask = windll.kernel32.GetLogicalDrives()
    for letter in string.ascii_uppercase:
        if bitmask & 1:
            drives.append(letter)
        bitmask >>= 1

    return drives

def init_file():
    path = Path(__file__).parent.parent / "log/func.log"
    file = path.open('a')
    return file

def main():
    file = init_file()
    for element in get_drives():
        drive=shutil.disk_usage(element+":")
        percentUsed=int(drive.used/drive.total*10000)/100
        absoluteFree=int(drive.free/8/1024/1024/1024)
        if percentUsed >= 90 or absoluteFree <=5:
            stage="CRIT"
        elif percentUsed >= 75:
            stage="WARN"
        else:
            stage=" OK "
        file.write(isodatetime+" "+hostName+" ["+stage+"] ["+element+":] disk usage: "+str(percentUsed)+"% remaining space: "+str(absoluteFree)+"GiB \n")

    file.close()

main()